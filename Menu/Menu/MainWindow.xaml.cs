﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using SharpDX.XInput;
namespace Menu
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        event EventHandler ProcessStartedHandler;
        Dictionary<string, string> NamesAndExes = new Dictionary<string, string>();
        FontFamily fontFamily = new FontFamily("8BIT WONDER(RUS BY LYAJKA) Nominal");
        About aboutWindow;
        Process CurrentProcess = null;
        List<Button> buttons = new List<Button>();
        int CurrentButton = 0;
        SharpDX.XInput.Gamepad Prevstate;
        DispatcherTimer timer = new DispatcherTimer();
        private Controller controller;
        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]



        static extern bool SetForegroundWindow(IntPtr hWnd);

        public MainWindow()
        {
            InitializeComponent();
            videoPlayer.Source = new Uri("background_dendy.mp4", UriKind.Relative);
            videoPlayer.MediaEnded += VideoPlayer_MediaEnded;
            NamesAndExes.Add("PacMan", @"PacMan\Pacman.exe");
            NamesAndExes.Add("Arcanoid", @"Arcanoid\Arkanoid.exe");
            NamesAndExes.Add("AfterBurner", @"Afterburner\My Project.exe");
            ViewMenu.GenerationButton(GridMenu, NamesAndExes, StartGame_Click, Button_GotFocus, Button_LostFocus, buttons, Click_Exit, OpenAbout);
            InitializeJoystick();
            Prevstate = new Gamepad();
            timer.Tick += TickTimer;
            timer.Interval = new TimeSpan(1000);
            timer.Start();
            buttons[0].Focus();

        }

        private void TickTimer(object sender, EventArgs e)
        {
            var keyboard = Keyboard.IsKeyDown(Key.Escape);
            if (controller != null && controller.IsConnected)
            {
                var state = controller.GetState().Gamepad;
                
                // Пример обработки нажатия кнопки на геймпаде
                if (((state.Buttons & GamepadButtonFlags.X) != 0) && ((Prevstate.Buttons & GamepadButtonFlags.X) == 0))
                {
                    if (aboutWindow != null) return;
                    if (buttons.Count - CurrentButton == 1) CurrentButton = 0;
                    else CurrentButton += 1;
                    buttons[CurrentButton].Focus();
                    Thread.Sleep(200);
                }
                if (((state.Buttons & GamepadButtonFlags.Y) != 0) && ((Prevstate.Buttons & GamepadButtonFlags.Y) == 0))
                {
                    if (aboutWindow != null) return;
                    buttons[CurrentButton].RaiseEvent(new RoutedEventArgs(Button.ClickEvent, buttons[CurrentButton]));
                    
                }
                if (((state.Buttons & GamepadButtonFlags.B) != 0) && ((Prevstate.Buttons & GamepadButtonFlags.B) == 0) || keyboard)
                {
                    if (aboutWindow != null) aboutWindow.Close();
                    aboutWindow = null;

                }

            }

        }

        private void OpenAbout(object sender, RoutedEventArgs e)
        {
            if (aboutWindow != null) return;
            aboutWindow = new About();
            aboutWindow.Show();
            Thread.Sleep(200);
        }
        private void Window_Closed_1(object sender, EventArgs e)
        {

        }
        private void Window_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {

        }
        private void Click_Exit(object sender, RoutedEventArgs e)
        {
            Process.Start("shutdown", "/s /t 0");
        }
        private void InitializeJoystick()
        {
            controller = new Controller(UserIndex.One);

            if (!controller.IsConnected)
            {
                MessageBox.Show("Геймпад не подключен");
            }
        }

        private void StartGame_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string currentDirectory = Directory.GetCurrentDirectory();
                string RelativePathToExe = (sender as Button).Tag.ToString();

                Process process = new Process();
                process.StartInfo.FileName = currentDirectory + @"\" + RelativePathToExe;
                if (CurrentProcess == null)
                {
                    process.Start();
                    CurrentProcess = process;
                }
                else if (CurrentProcess.HasExited == true)
                {
                    process.Start();
                    CurrentProcess = process;
                }
                //ProcStartHand(false);

                if (CurrentProcess != null) SetForegroundWindow(CurrentProcess.MainWindowHandle);

            }
            catch (Exception i)
            {
                Console.WriteLine(i.Message);
            }


        }
        private void VideoPlayer_MediaEnded(object sender, RoutedEventArgs e)
        {
            // Повторите видео после его завершения (если необходимо)
            videoPlayer.Position = TimeSpan.Zero;
            videoPlayer.Play();
        }

        private void Button_GotFocus(object sender, RoutedEventArgs e)
        {
            // Получаем кнопку, которая была фокусирована
            if (sender is Button button)
            {
                // Создаем новый стиль или изменяем существующий стиль
                Style focusedStyle = new Style(typeof(Button));
                focusedStyle.Setters.Add(new Setter(Button.FontFamilyProperty, fontFamily));
                focusedStyle.Setters.Add(new Setter(Button.BackgroundProperty, Brushes.Black));
                focusedStyle.Setters.Add(new Setter(Button.ForegroundProperty, Brushes.White));
                focusedStyle.Setters.Add(new Setter(Button.FontSizeProperty, (double)35));
                focusedStyle.Setters.Add(new Setter(Button.BorderBrushProperty, Brushes.Transparent));

                // Применяем стиль к кнопке
                button.Style = focusedStyle;
                button.FocusVisualStyle = null;
            }
        }

        private void Button_LostFocus(object sender, RoutedEventArgs e)
        {
            // Получаем кнопку, которая потеряла фокус
            if (sender is Button button)
            {

                // Возвращаем стандартный стиль (или создайте новый стиль)
                Style defaultStyle = new Style(typeof(Button));
                defaultStyle.Setters.Add(new Setter(Button.FontFamilyProperty, fontFamily));
                defaultStyle.Setters.Add(new Setter(Button.BackgroundProperty, Brushes.Transparent));
                defaultStyle.Setters.Add(new Setter(Button.ForegroundProperty, Brushes.Black));
                defaultStyle.Setters.Add(new Setter(Button.FontSizeProperty, (double)35));
                defaultStyle.Setters.Add(new Setter(Button.BorderBrushProperty, Brushes.Transparent));

                // Применяем стиль к кнопке
                button.Style = defaultStyle;
                button.FocusVisualStyle = null;
            }
        }


    }
}
