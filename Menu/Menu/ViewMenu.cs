﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Media3D;

namespace Menu
{
    public static class ViewMenu
    {
        static int WidthButtons = 400;
        static int HeightButtons = 60;
        static int MarginButtons = HeightButtons * 3;
        static FontFamily fontFamily = new FontFamily("8BIT WONDER(RUS BY LYAJKA) Nominal");

        static int coefMarge = 0;
        

        public static void GenerationButton(Grid GridMenu, Dictionary<string,string> NamesAndExes, RoutedEventHandler MethodCLick,RoutedEventHandler GotFocus, RoutedEventHandler Lost_Focus,List<Button> ButtonsList, RoutedEventHandler Click_Exit,RoutedEventHandler OpenAbout)
        {
            Style focusedStyle = new Style(typeof(Button));
            focusedStyle.Setters.Add(new Setter(Button.FontFamilyProperty, fontFamily));
            focusedStyle.Setters.Add(new Setter(Button.BackgroundProperty, Brushes.Transparent));
            focusedStyle.Setters.Add(new Setter(Button.ForegroundProperty, Brushes.Black));
            focusedStyle.Setters.Add(new Setter(Button.FontSizeProperty, (double)35));
            focusedStyle.Setters.Add(new Setter(Button.BorderBrushProperty, Brushes.Transparent));
            
            foreach (KeyValuePair<string,string> NAM in NamesAndExes)
            {

                Button button = new Button()
                {
                    Height = HeightButtons,
                    Width = WidthButtons,
                    VerticalAlignment = VerticalAlignment.Center,
                    HorizontalAlignment = HorizontalAlignment.Center,
                    Margin = new Thickness(0, coefMarge * MarginButtons + 10, 0, 90),
                    Content = NAM.Key,
                    Tag = NAM.Value,
                    BorderThickness = new Thickness(0,0,0,0)
                };
                coefMarge++;
                
                button.Click += MethodCLick;
                button.GotFocus += GotFocus;
                button.LostFocus += Lost_Focus;
                // Применяем стиль к кнопке
                button.Style = focusedStyle;
                button.FocusVisualStyle = null;
                ButtonsList.Add(button);
                GridMenu.Children.Add(button);
            }           

            Button but = new Button()
            {
                Height = HeightButtons,
                Width = WidthButtons,
                VerticalAlignment = VerticalAlignment.Center,
                HorizontalAlignment = HorizontalAlignment.Center,
                Margin = new Thickness(0, coefMarge * MarginButtons + 10, 0, 90),
                Content = "О нас",
            };
            but.Style = focusedStyle;
            but.Click += OpenAbout;
            but.GotFocus += GotFocus;
            but.LostFocus += Lost_Focus;
            ButtonsList.Add(but);
            GridMenu.Children.Add(but);

            but = new Button()
            {
                Height = HeightButtons,
                Width = WidthButtons,
                VerticalAlignment = VerticalAlignment.Center,
                HorizontalAlignment = HorizontalAlignment.Center,
                Margin = new Thickness(0, (coefMarge + 1) * MarginButtons + 10, 0, 90),
                Content = "Exit",


            };
            but.Style = focusedStyle;
            but.Click += Click_Exit;
            but.GotFocus += GotFocus;
            but.LostFocus += Lost_Focus;
            ButtonsList.Add(but);
            GridMenu.Children.Add(but);
        }

    }
}
